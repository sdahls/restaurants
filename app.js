const express = require('express');
const app = express();
const { PORT = 8080 } = process.env;
const path = require('path');
//importing modules

app.use('/assets', express.static(path.join(__dirname, 'public', 'static')))

app.get('/',(req, res) =>{
    console.log("indexpage entered")
    return res.sendFile(path.join(__dirname,'public', 'index.html'))
})

 app.get('/data',(req, res)=>{
    console.log("data has been fethced")
     return res.sendFile(path.join(__dirname,'data.json'))

 })

app.get('/restaurants', (req, res) => {
    console.log("restaurant page enterded")
    return res.sendFile(path.join(__dirname, 'public', 'restaurants.html'));

})

app.listen(PORT, ()=> console.log(`server started on port ${PORT}`))